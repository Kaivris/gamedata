﻿using System;
using UnityEngine;

[Serializable]
public class RabbitSave : Save
{

    public Data data;
    private RabbitScript rabbit;
    private string jsonString;

    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 eulerAngles;
        public bool move;
        public bool visible;
    }

    void Awake()
    {
        rabbit = GetComponent<RabbitScript>();
        data = new Data();
    }

    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = rabbit.transform.position;
        data.eulerAngles = rabbit.transform.eulerAngles;
        data.move = rabbit.move;
        data.visible = rabbit.visible;

        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        rabbit.transform.position = data.position;
        rabbit.transform.eulerAngles = data.eulerAngles;
        rabbit.move = data.move;
        rabbit.visible = data.visible;
        rabbit.transform.parent = GameObject.Find("Rabbits").transform;
        
    }
}