﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }

    //add objects to saved item list
    public void AddObject(string item) {
        saveItems.Add(item);
    }

    //clear previously saved items from list
    public void Save() {
        saveItems.Clear();

        //Add saveable objects to an array
        //Go through array and serialize objects
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            saveItems.Add(saveableObject.Serialize());
        }

        //write a file that has each serialized string of saved items
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }
    
    //if its the first play of the game (with no save data) clear the list of saved items, load the current scene
    public void Load() {
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (firstPlay) return;
        //load saved data to strings
        LoadSaveGameData();
        //destroy all of the objects that are going to be loaded
        DestroyAllSaveableObjectsInScene();
        //load saved objects from strings
        CreateGameObjects();
    }
    //read file with save data
    void LoadSaveGameData() {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            //as long as there is another character to read
            while (gameDataFileStream.Peek() >= 0) {
                //put the next line to a string, removing white spaces
                string line = gameDataFileStream.ReadLine().Trim();
                //if there are characters in the string, add the string to list of saved items
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

    //clear all saveable objects from the scene
    void DestroyAllSaveableObjectsInScene() {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }

    //for each saved object string
    void CreateGameObjects() {
        foreach (string saveItem in saveItems) {
            //make a string for the prefab template
            string pattern = @"""prefabName"":""";
            //find the first occurance of the prefab template
            int patternIndex = saveItem.IndexOf(pattern);

            //load the prefab strings as gameobjects, assign properties for the loaded item from their save script
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);
            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
