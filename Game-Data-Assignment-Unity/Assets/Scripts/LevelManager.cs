﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    //transform of the gameobjects that hold the tile gameobjects from each layer
    public Transform groundHolder;
    public Transform turretHolder;
    public Transform rabbitHolder;

    //different cases for the data formats the map could be encoded in
    public enum MapDataFormat {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject rabbitPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> rabbitPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    //the TMV file name. As it says.
    public string TMXFilename;

    //strings needed to read the files and directories 
    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    //how many pixels per our personal Unity Unit
    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    //Clear the console of debug messages
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }



    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel() {

        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
        DestroyChildren(rabbitHolder);

        //Find the file for the map directory and the TMX file.
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        /* Clear the map data list. Read the TMX file. Convert the map columns and rows to useable values of width and height. 
         * Do the same with the tile width and heights, the total tile count, and amount of columns. Calculate the number of rows needed for the tile sheet.
         * Check how the map is encoded (CSV or Base64) and run the corresponding case.
         */
        {
            mapData.Clear();

            string content = File.ReadAllText(TMXFile);

            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;


                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));


                reader.ReadToFollowing("layer");


                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");

                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }
                //read the map data, return it as a string and then clean up the white-space characters from that string
                mapDataString = reader.ReadElementContentAsString().Trim();

                //clear the list of turret & rabbit positions
                turretPositions.Clear();
                rabbitPositions.Clear();
               //Find the x and y attributes of each object. Convert that to the size of our unity unit (as specified by pixels per unit)
               //add the turret and rabbit positions to their lists of positions.
                if (reader.ReadToFollowing("objectgroup")) {
                    if (reader.ReadToDescendant("object")) {
                        do {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            turretPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
         
                    }
                }

                if (reader.ReadToFollowing("objectgroup"))
                {
                    if (reader.ReadToDescendant("object"))
                    {
                        do
                        {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            rabbitPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));

                    }
                }

            }

            //Depending on how the map data is encoded...
            switch (mapDataFormat) {

                //if the map data is encoded in Base64...
                case MapDataFormat.Base64:
                    
                    //convert the map data string (in Base64) to a byte array
                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    //Go through the byte array. Find the tile ID for each tile. 
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;

                    //if the map data is encoded in CSV, do the same thing but converting from CSV format
                case MapDataFormat.CSV:
                    
                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {
            //calculate the tile width and height by dividing the values in pixels by the number of pixles in our Unity Unit.
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);
            
            //The offset for each tile from the others
            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            //The offset of the tiles from the center of the map
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // Create a new empty texture. Load the sprite sheet into the texture. Change the filtermode to 'point' mode (better for pixel art). 
        // Change the texture wrap mode to 'clamp' mode (clamps the texture to the edge, without tiling or mirroring).
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Clear the list of map sprites. For each row and column on the sprite sheet, create the new sprites and add it to the list of map sprites.
        {
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // Clear the list of Tiles. For each row and column on the map, instantiate new tile game objects. Assign them a sprite from the list of map sprites according to their tile ID. 
        // Make the tiles a child of the groundHolder object, and add them to the list of Tiles. 
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // Instantiate a turret game object at every turret position. Call it a turret and make it a child of the TurretHolder object.
        {
            foreach (Vector3 turretPosition in turretPositions) {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }

        // the same thing as the turrets, but for rabbits
        {
            foreach (Vector3 rabbitPosition in rabbitPositions)
            {
                GameObject rabbit = Instantiate(rabbitPrefab, rabbitPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                rabbit.name = "Rabbit";
                rabbit.transform.parent = rabbitHolder;
            }
        }

        //Print the current time to the console
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


