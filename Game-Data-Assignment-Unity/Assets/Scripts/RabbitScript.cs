﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitScript : MonoBehaviour
{
    public int runSpeed;
    public GameObject Target;
    public bool move = false;
    public bool visible = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (move == true)
        {
            Debug.Log(transform.position);
            Debug.Log(Target.transform.position);

            transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, runSpeed * Time.deltaTime);
        }
        if (visible == false)
        {
            GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            move = true;
        
        }
        if (other.tag == "Hole")
        {
            move = false;
            visible = false;
        }

    }

    

}
